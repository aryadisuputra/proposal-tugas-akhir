
#include <iostream>
#include <string>
#include <fstream>
#include <iomanip>

#include "ns3/core-module.h"
#include "ns3/network-module.h"
#include "ns3/internet-module.h"
#include "ns3/point-to-point-module.h"
#include "ns3/applications-module.h"
#include "ns3/netanim-module.h"
#include "ns3/animation-interface.h"
#include "ns3/packet-sink-helper.h"
#include "ns3/uinteger.h"
#include "ns3/mobility-module.h"
#include "ns3/olsr-helper.h"
#include "ns3/ipv4-static-routing-helper.h"
#include "ns3/ipv4-list-routing-helper.h"
#include "ns3/wifi-module.h"
#include "ns3/gnuplot.h"
#include "ns3/flow-monitor-module.h"
#include "ns3/aodv-module.h"
#include "ns3/config-store-module.h"
#include "ns3/wifi-mac.h"
#include "ns3/pointer.h"
#include "ns3/boolean.h"

//#include "crypto++/skipjack.h"
#include "crypto++/modes.h"
#include "crypto++/aes.h"
#include "crypto++/filters.h"
#include "crypto++/integer.h"


#include "ns3/wifi-80211p-helper.h"
#include "ns3/wave-mac-helper.h"


NS_LOG_COMPONENT_DEFINE ("auth-LLSP");

using namespace ns3;
using namespace CryptoPP;
using namespace std;
//static inline std::string
//PrintReceivedPacket (Address& from)
//{
//  InetSocketAddress iaddr = InetSocketAddress::ConvertFrom (from);
//
//  std::ostringstream oss;
//  oss << "--\nReceived one packet! Socket: " << iaddr.GetIpv4 ()
//      << " port: " << iaddr.GetPort ()
//      << " at time = " << Simulator::Now ().GetSeconds ()
//      << "\n--";
//
//  return oss.str ();
//}

/**
 * \param socket Pointer to socket.
 *
 * Packet receiving sink.
 */

/**
 * \param socket Pointer to socket.
 * \param pktSize Packet size.
 * \param n Pointer to node.
 * \param pktCount Number of packets to generate.
 * \param pktInterval Packet sending interval.
 *
 * Traffic generator.
 */
static void GenerateTraffic(Ptr<Socket> socket, uint32_t size) {
	std::cout << "at=" << Simulator::Now().GetSeconds() << "s, tx bytes="
			<< size << std::endl;
	socket->Send(Create<Packet>(size));
	if (size > 0) {
		Simulator::Schedule(Seconds(0.5), &GenerateTraffic, socket, size - 50);
	} else {
		socket->Close();
	}
}

//Authentication function---------------------------------------------------------

static inline std::string PrintReceivedPacket(Ptr<Socket> socket,
		Ptr<Packet> packet, Address senderAddress) {
	std::ostringstream oss;

	oss << Simulator::Now().GetSeconds() << " " << socket->GetNode()->GetId();

	if (InetSocketAddress::IsMatchingType(senderAddress)) {
		InetSocketAddress address = InetSocketAddress::ConvertFrom(senderAddress);
		oss << " Valid packet from " << address.GetIpv4();
	} else {
		oss << " Invalid packet [Drop Packet] ";
	}
	return oss.str();
}

void checkAuthentication(Ptr<Socket> socket) {
	uint32_t bytesTotal;
	uint32_t packetsReceived;

	Ptr<Packet> packet;
	Address senderAddress;

	bytesTotal = 0;
	packetsReceived = 0;

	while ((packet = socket->RecvFrom(senderAddress))) {
		bytesTotal += packet->GetSize();
		packetsReceived += 1;
		NS_LOG_UNCOND(PrintReceivedPacket(socket, packet, senderAddress));
	}
}

//-------------------------------------------------------------------------------

int
main (int argc, char *argv[])
{
  std::string phyMode ("DsssRate2Mbps");
  double distance = 100;
  double Prss = -80;            // dBm
  uint32_t PpacketSize = 500;   // bytes 500 bytes=4000 bits
  bool verbose = false;
//  bool tracing = false;

  uint32_t numNode = 15;
  uint32_t fakeNode = 3;


  // simulation parameters
  uint32_t numPackets = 10;  // number of packets to send
  double interval = 1;          // seconds
  double startTime = 0.0;       // seconds
  double distanceToRx = 100.0;  // meters
  /*
   * This is a magic number used to set the transmit power, based on other
   * configuration.
   */
  double offset = 81;

  CommandLine cmd;
  cmd.AddValue ("phyMode", "Wifi Phy mode", phyMode);
  cmd.AddValue ("Prss", "Intended primary RSS (dBm)", Prss);
  cmd.AddValue ("PpacketSize", "size of application packet sent", PpacketSize);
  cmd.AddValue ("numPackets", "Total number of packets to send", numPackets);
  cmd.AddValue ("startTime", "Simulation start time", startTime);
  cmd.AddValue ("distanceToRx", "X-Axis distance between nodes", distanceToRx);
  cmd.AddValue ("verbose", "Turn on all device log components", verbose);
  cmd.Parse (argc, argv);

  // Convert to time object
  Time interPacketInterval = Seconds (interval);

  // disable fragmentation for frames below 2200 bytes
  Config::SetDefault ("ns3::WifiRemoteStationManager::FragmentationThreshold",
                      StringValue ("2200"));
  // turn off RTS/CTS for frames below 2200 bytes
  Config::SetDefault ("ns3::WifiRemoteStationManager::RtsCtsThreshold",
                      StringValue ("2200"));
  // Fix non-unicast data rate to be the same as that of unicast
  Config::SetDefault ("ns3::WifiRemoteStationManager::NonUnicastMode",
                      StringValue (phyMode));

// code for encryption -----------------------------------------------------------


  string msg = "pesan asli";
  string cipher;
  string recovered;

  byte key[CryptoPP::AES::DEFAULT_KEYLENGTH],
  	  	  iv[CryptoPP::AES::BLOCKSIZE];
  memset(key, 0x00, CryptoPP::AES::DEFAULT_KEYLENGTH);
  memset(iv, 0x00, CryptoPP::AES::BLOCKSIZE);
  /*-------------------cipher text---------------------*/
  CryptoPP::AES::Encryption aes(key, CryptoPP::AES::DEFAULT_KEYLENGTH);
  CryptoPP::CBC_Mode_ExternalCipher::Encryption cbcMAC(aes, iv);

  CryptoPP::StreamTransformationFilter stfEncryptor(cbcMAC, new CryptoPP::StringSink(cipher));
  stfEncryptor.Put(reinterpret_cast<const unsigned char*>(msg.c_str()), msg.length() + 1);
  stfEncryptor.MessageEnd();

  //dump cipher text
  for (uint32_t i = 0; i < cipher.size(); i++) {
	  cout << "0x" << std::hex << (0xFF & static_cast<byte>(cipher[i])) << endl;
  }

  //decryption
  CryptoPP::AES::Decryption aesDec(key, CryptoPP::AES::DEFAULT_KEYLENGTH);
  CryptoPP::CBC_Mode_ExternalCipher::Decryption cbcMACDec(aesDec, iv);

  CryptoPP::StreamTransformationFilter stfDecryptor(cbcMACDec, new CryptoPP::StringSink(recovered));
  stfDecryptor.Put(reinterpret_cast<const unsigned char*>(cipher.c_str()), cipher.size());
  stfDecryptor.MessageEnd();

  ns3::PacketMetadata::Enable();

  /********************************************************/

// end of encryption code --------------------------------------------------------


  NodeContainer c;
  c.Create (numNode);     // create 15 nodes
  NodeContainer networkNodes;
  networkNodes.Add (c.Get (0));
  networkNodes.Add (c.Get (1));
  networkNodes.Add (c.Get (2));
  networkNodes.Add (c.Get (3));
  networkNodes.Add (c.Get (4));
  networkNodes.Add (c.Get (5));
  networkNodes.Add (c.Get (6));
  networkNodes.Add (c.Get (7));
  networkNodes.Add (c.Get (8));
  networkNodes.Add (c.Get (9));
  networkNodes.Add (c.Get (10));
  networkNodes.Add (c.Get (11));
  networkNodes.Add (c.Get (12));
  networkNodes.Add (c.Get (13));
  networkNodes.Add (c.Get (14));

  NodeContainer f;
  f.Create(fakeNode);

  // The below set of helpers will help us to put together the wifi NICs we want
  WifiHelper wifi;
  if (verbose)
    {
      wifi.EnableLogComponents ();
    }
  wifi.SetStandard (WIFI_PHY_STANDARD_80211b);

  /** Wifi PHY **/
  /***************************************************************************/
  YansWifiPhyHelper wifiPhy = YansWifiPhyHelper::Default ();
  wifiPhy.Set ("RxGain", DoubleValue (-10));
  wifiPhy.Set ("TxGain", DoubleValue (offset + Prss-15.0));
  wifiPhy.Set ("CcaMode1Threshold", DoubleValue (0.0));
  /***************************************************************************/

  /** wifi channel **/
  YansWifiChannelHelper wifiChannel;
  wifiChannel.SetPropagationDelay ("ns3::ConstantSpeedPropagationDelayModel");
  wifiChannel.AddPropagationLoss ("ns3::FriisPropagationLossModel");
  // create wifi channel
  Ptr<YansWifiChannel> wifiChannelPtr = wifiChannel.Create ();
  wifiPhy.SetChannel (wifiChannelPtr);
  wifiPhy.SetPcapDataLinkType(YansWifiPhyHelper::DLT_IEEE802_11_RADIO);

  /** MAC layer **/
  // Add a non-QoS upper MAC, and disable rate control
  NqosWifiMacHelper wifiMac = NqosWifiMacHelper::Default ();
  wifi.SetRemoteStationManager ("ns3::ConstantRateWifiManager", "DataMode",
                                StringValue (phyMode), "ControlMode",
                                StringValue (phyMode));
  // Set it to ad-hoc mode
  wifiMac.SetType ("ns3::AdhocWifiMac");

  /** install PHY + MAC **/
  NetDeviceContainer devices = wifi.Install (wifiPhy, wifiMac, networkNodes);
  NetDeviceContainer fakeDevices = wifi.Install(wifiPhy, wifiMac, f);

  /** mobility **/
  MobilityHelper mobility;
  mobility.SetPositionAllocator ("ns3::GridPositionAllocator",
                                 "MinX", DoubleValue (0.0),
                                 "MinY", DoubleValue (0.0),
                                 "DeltaX", DoubleValue (distance),
                                 "DeltaY", DoubleValue (distance),
                                 "GridWidth", UintegerValue (5),
                                 "LayoutType", StringValue ("RowFirst"));
  mobility.SetMobilityModel ("ns3::ConstantPositionMobilityModel");
  mobility.Install (c);
  mobility.Install(f);

  // Enable AODV for fakeNodes
  AodvHelper aodv;
Ipv4StaticRoutingHelper staticRouting;

  Ipv4ListRoutingHelper list;
    list.Add (staticRouting, 0);
    list.Add (aodv, 10);
InternetStackHelper internet;
    internet.SetRoutingHelper (list);
    internet.Install (networkNodes);

  InternetStackHelper aodvInternet;
  aodvInternet.SetRoutingHelper(aodv);
  aodvInternet.Install(f);
  


  /***************************************************************************/

  /** Internet stack **/
//  InternetStackHelper internet;
//  internet.Install (networkNodes);

  Ipv4AddressHelper ipv4;
  NS_LOG_INFO ("Assign IP Addresses.");
  ipv4.SetBase ("10.1.1.0", "255.255.255.0");
  Ipv4InterfaceContainer i = ipv4.Assign (devices);
  Ipv4InterfaceContainer j = ipv4.Assign(fakeDevices);
  UdpEchoServerHelper echoServer(91);

  TypeId tid = TypeId::LookupByName ("ns3::UdpSocketFactory");
  Ptr<Socket> recvSink = Socket::CreateSocket (networkNodes.Get (14), tid);  // node 14, receiver
  InetSocketAddress local = InetSocketAddress (Ipv4Address::GetAny (), 80);
  recvSink->Bind (local);
  recvSink->SetRecvCallback (MakeCallback (&checkAuthentication));

  Ptr<Socket> source = Socket::CreateSocket (networkNodes.Get (0), tid);    // node 0, sender
  InetSocketAddress remote = InetSocketAddress (i.GetAddress(14,0), 80);
  source->SetAllowBroadcast (true);
  source->Connect (remote);

  //-------------------------------------------------------------------------
    ApplicationContainer serverApps =  echoServer.Install(c.Get(4));
  serverApps.Start(Seconds(1.0));
  serverApps.Stop(Seconds(200.0));

  UdpEchoClientHelper echoClient(i.GetAddress(4), 91);
  echoClient.SetAttribute("MaxPackets", UintegerValue(1000));
  echoClient.SetAttribute("Interval", TimeValue(Seconds(2.0)));

  ApplicationContainer client_app_0 = echoClient.Install(c.Get(0));
  client_app_0.Start(Seconds(2.0));
  client_app_0.Stop(Seconds(200.0));

  ApplicationContainer client_app_1 = echoClient.Install(c.Get(1));
  client_app_1.Start(Seconds(2.0));
  client_app_1.Stop(Seconds(200.0));

  ApplicationContainer client_app_2 = echoClient.Install(c.Get(2));
  client_app_2.Start(Seconds(2.0));
  client_app_2.Stop(Seconds(200.0));

  ApplicationContainer client_app_3 = echoClient.Install(c.Get(3));
  client_app_3.Start(Seconds(2.0));
  client_app_3.Stop(Seconds(200.0));

  ApplicationContainer client_app_4 = echoClient.Install(c.Get(4));
  client_app_4.Start(Seconds(2.0));
  client_app_4.Stop(Seconds(200.0));

  ApplicationContainer client_app_5 = echoClient.Install(c.Get(5));
  client_app_5.Start(Seconds(2.0));
  client_app_5.Stop(Seconds(200.0));

  ApplicationContainer client_app_6 = echoClient.Install(c.Get(6));
  client_app_6.Start(Seconds(2.0));
  client_app_6.Stop(Seconds(200.0));

  ApplicationContainer client_app_7 = echoClient.Install(c.Get(7));
  client_app_7.Start(Seconds(2.0));
  client_app_7.Stop(Seconds(200.0));

  ApplicationContainer client_app_8 = echoClient.Install(c.Get(8));
  client_app_8.Start(Seconds(2.0));
  client_app_8.Stop(Seconds(200.0));

  ApplicationContainer client_app_9 = echoClient.Install(c.Get(9));
  client_app_9.Start(Seconds(2.0));
  client_app_9.Stop(Seconds(200.0));

  ApplicationContainer client_app_10 = echoClient.Install(c.Get(10));
  client_app_10.Start(Seconds(2.0));
  client_app_10.Stop(Seconds(200.0));

  ApplicationContainer client_app_11 = echoClient.Install(c.Get(11));
  client_app_11.Start(Seconds(2.0));
  client_app_11.Stop(Seconds(200.0));

  ApplicationContainer client_app_12 = echoClient.Install(c.Get(12));
  client_app_12.Start(Seconds(2.0));
  client_app_12.Stop(Seconds(200.0));

  ApplicationContainer client_app_13 = echoClient.Install(c.Get(13));
  client_app_13.Start(Seconds(2.0));
  client_app_13.Stop(Seconds(200.0));

  ApplicationContainer client_app_14 = echoClient.Install(c.Get(14));
  client_app_14.Start(Seconds(2.0));
  client_app_14.Stop(Seconds(200.0));


  //----------------connecting fakeNodes to network-------------------
  UdpEchoClientHelper fakeClient(i.GetAddress(4), 91);
  fakeClient.SetAttribute("MaxPackets", UintegerValue(1000));
  fakeClient.SetAttribute("Interval", TimeValue(Seconds(2.0)));

  ApplicationContainer fake_app_0 = fakeClient.Install(f.Get(0));
  fake_app_0.Start(Seconds(2.0));
  fake_app_0.Stop(Seconds(200.0));

  ApplicationContainer fake_app_1 = fakeClient.Install(f.Get(1));
  fake_app_1.Start(Seconds(2.0));
  fake_app_1.Stop(Seconds(200.0));

  ApplicationContainer fake_app_2 = fakeClient.Install(f.Get(2));
  fake_app_2.Start(Seconds(2.0));
  fake_app_2.Stop(Seconds(200.0));

  Ipv4GlobalRoutingHelper::PopulateRoutingTables();

  //-implement encryption-
 echoClient.SetFill(client_app_0.Get(0), cipher);
  echoClient.SetFill(client_app_1.Get(0), cipher);
  echoClient.SetFill(client_app_2.Get(0), cipher);
  echoClient.SetFill(client_app_3.Get(0), cipher);
  echoClient.SetFill(client_app_4.Get(0), cipher);
  echoClient.SetFill(client_app_5.Get(0), cipher);
  echoClient.SetFill(client_app_6.Get(0), cipher);
  echoClient.SetFill(client_app_7.Get(0), cipher);
  echoClient.SetFill(client_app_8.Get(0), cipher);
  echoClient.SetFill(client_app_9.Get(0), cipher);
  echoClient.SetFill(client_app_10.Get(0), cipher);
  echoClient.SetFill(client_app_11.Get(0), cipher);
  echoClient.SetFill(client_app_12.Get(0), cipher);
  echoClient.SetFill(client_app_13.Get(0), cipher);
  echoClient.SetFill(client_app_14.Get(0), cipher);
  /*-----------------------------------------------*/

  fakeClient.SetFill(fake_app_0.Get(0), "Pesan palsu");
  fakeClient.SetFill(fake_app_1.Get(0), "Pesan palsu");
  fakeClient.SetFill(fake_app_2.Get(0), "Pesan palsu");

//  if (tracing == true)
//    {
//      AsciiTraceHelper ascii;
//      wifiPhy.EnableAsciiAll (ascii.CreateFileStream ("wifi-simple-adhoc-grid.tr"));
      wifiPhy.EnablePcap ("auth-LLSP", devices);
      // Trace routing tables
//      Ptr<OutputStreamWrapper> routingStream = Create<OutputStreamWrapper> ("wifi-simple-adhoc-grid.routes", std::ios::out);
//      olsr.PrintRoutingTableAllEvery (Seconds (2), routingStream);
//
//      // To do-- enable an IP-level trace that shows forwarding events only
//    }


  /***************************************************************************/

  //Output
//  AsciiTraceHelper ascii;
//  wifiPhy.EnableAsciiAll(ascii.CreateFileStream("wireless_AES.tr"));
//  wifiPhy.EnablePcap("Pcap_AES", devices);

//  Ptr<OutputStreamWrapper> routingStream = Create<OutputStreamWrapper>("rute", std::ios::out);
//  olsr.PrintRoutingTableAllEvery(Seconds(2.0),routingStream);

   /*Animation setup*/
  AnimationInterface anm("auth-LLSP.xml");

  for (uint32_t x=0; x<=2; x++) {
	  anm.UpdateNodeColor(f.Get(x), 255, 0, 0);
  }

  for (uint32_t y=0; y<=14; y++) {
	  anm.UpdateNodeColor(c.Get(y), 0, 0, 255);
  }

  anm.SetConstantPosition(c.Get(0),100.0, 100.0);
  anm.SetConstantPosition(c.Get(1),200.0, 50.0);
  anm.SetConstantPosition(c.Get(2),250.0, 85.0);
  anm.SetConstantPosition(c.Get(3),200.0, 150.0);
  anm.SetConstantPosition(c.Get(4),275.0, 25.0);
  anm.SetConstantPosition(c.Get(5),135.0, 315.0);
  anm.SetConstantPosition(c.Get(6),175.0, 200.0);
  anm.SetConstantPosition(c.Get(7),100.0, 325.0);
  anm.SetConstantPosition(c.Get(8),375.0, 115.0);
  anm.SetConstantPosition(c.Get(9),265.0, 175.0);
  anm.SetConstantPosition(c.Get(10),250.0, 285.0);
  anm.SetConstantPosition(c.Get(11),300.0, 200.0);
  anm.SetConstantPosition(c.Get(12),350.0, 250.0);
  anm.SetConstantPosition(c.Get(13),200.0, 300.0);
  anm.SetConstantPosition(c.Get(14),192.0, 350.0);

  //-------------------------------------------------

  anm.SetConstantPosition(f.Get(0), 200.0, 177.0);
  anm.SetConstantPosition(f.Get(1), 100.0, 150.0);
  anm.SetConstantPosition(f.Get(2), 300.0, 350.0);




  /** simulation setup **/
  // start traffic
/*  Simulator::Schedule (Seconds (startTime), &GenerateTraffic, source, PpacketSize,
                       networkNodes.Get (0), numPackets, interPacketInterval);*/

  Simulator::Stop (Seconds (1500.0));
  Simulator::Run ();
  Simulator::Destroy ();

  return 0;
}
